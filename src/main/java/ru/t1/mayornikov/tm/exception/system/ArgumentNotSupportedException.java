package ru.t1.mayornikov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException{

    public ArgumentNotSupportedException() {
        super("Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Argument \"" + argument + "\" is not supported...");
    }

}