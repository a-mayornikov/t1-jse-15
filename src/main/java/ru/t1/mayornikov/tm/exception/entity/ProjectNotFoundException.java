package ru.t1.mayornikov.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityException{

    public ProjectNotFoundException() {
        super("Project not found...");
    }

}