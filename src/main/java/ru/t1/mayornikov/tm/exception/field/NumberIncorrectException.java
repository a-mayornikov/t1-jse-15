package ru.t1.mayornikov.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException{

    public NumberIncorrectException() {
        super("This value is not number...");
    }

    public NumberIncorrectException(final String value, Throwable cause) {
        super("Value \"" + value + "\" is not number...");
    }

}