package ru.t1.mayornikov.tm.exception.field;

public final class IndexEmptyException extends AbstractFieldException{

    public IndexEmptyException() {
        super("[Index is empty...");
    }
    
}