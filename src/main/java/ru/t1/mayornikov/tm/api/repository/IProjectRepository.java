package ru.t1.mayornikov.tm.api.repository;

import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    boolean existsById(String id);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project create(String name, String description);

    Project create(String name);

    Project findOne(String id);

    Project findOne(Integer index);

    Project remove(Project project);

    Project remove(String id);

    Project remove(Integer index);

    Integer getSize();

}