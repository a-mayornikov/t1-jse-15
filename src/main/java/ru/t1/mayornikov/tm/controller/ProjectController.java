package ru.t1.mayornikov.tm.controller;

import ru.t1.mayornikov.tm.api.controller.IProjectController;
import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.api.service.IProjectTaskService;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final Status status = Status.toStatus(TerminalUtil.nextLine());
        projectService.changeProjectStatus(id, status);
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatus(index, status);
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatus(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatus(id, Status.COMPLETED);
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatus(index, Status.COMPLETED);
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");
        renderProjects();
    }

    @Override
    public void showProjectsSorted() {
        System.out.println("[SORTED PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final Sort sort = Sort.toSort(TerminalUtil.nextLine());
        renderProjects(projectService.findAll(sort));
    }

    private void renderProjects() {
        int index = 1;
        for (final Project project : projectService.findAll()) {
            if (project == null) continue;;
            System.out.println(index++ + ".");
            showProject(project);
        }
        if (index == 1) System.out.println("No one project found...");
    }

    private void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;;
            System.out.println(index++ + ".");
            showProject(project);
        }
        if (index == 1) System.out.println("No one project found...");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        final String id = project.getId();
        final String name = project.getName();
        final String description = project.getDescription();
        final Status status = project.getStatus();
        if (!"".equals(id)) System.out.println("ID: " + id);
        if (!"".equals(name)) System.out.println("NAME: " + name);
        if (!"".equals(description)) System.out.println("DESCRIPTION: " + description);
        if (status != null) System.out.println("STATUS: " + Status.toName(status));
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final Project project = projectService.findOne(TerminalUtil.nextLine());
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Project project = projectService.findOne(TerminalUtil.nextNumber() - 1);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final Project project = projectService.findOne(TerminalUtil.nextLine());
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Project project = projectService.findOne(TerminalUtil.nextNumber() - 1);
        showProject(project);
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final Project project = projectService.findOne(TerminalUtil.nextLine());
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Project project = projectService.findOne(TerminalUtil.nextNumber() - 1);
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        project.setName(name);
        project.setDescription(description);
    }

}